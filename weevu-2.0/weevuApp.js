var newConfig = function($routeProvider) { 
	$routeProvider
		.when('/', {
			controller: 'LinksController',
			templateUrl: 'view/home.html'
		})
		.when('/home/', {
			controller: 'LinksController',
			templateUrl: 'view/home.html'
		})
	;
};

var SeedApp = angular.module('weevuApp',['ngResource', 'ngRoute'] ).config(newConfig);

